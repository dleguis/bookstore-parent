package edu.flst.bookstore.presentation.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import edu.flst.bookstore.presentation.model.BookSearchModel;
import edu.flst.bookstore.presentation.model.CartArticle;
import edu.flst.bookstore.service.AuthorService;
import edu.flst.bookstore.service.BookService;
import edu.flst.bookstore.transverse.builder.BookCriteria;
import edu.flst.bookstore.transverse.dto.BookDto;
import edu.flst.bookstore.transverse.utils.binding.BindingResultUtils;

/**
 * Created by Dimitri on 19/10/14.
 */
@Controller
@RequestMapping(value = "/books")
public class BooksController extends AbstractController {

    /**
     * The Constant LOGGER.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(BooksController.class);

    /**
     * The book service.
     */
    @Autowired
    private BookService bookService;

    /** The author service. */
    @Autowired
    private AuthorService authorService;

    /**
     * Index.
     *
     * @param model
     *            the model
     * @return the string
     */
    @RequestMapping(value = "/catalog.html", method = RequestMethod.GET)
    public String index(Model model) {
	model.addAttribute("books", bookService.rechercher());
	model.addAttribute("authors", authorService.rechercher());
	model.addAttribute("bookSearchModel", new BookSearchModel());
	return "books";
    }

    /**
     * Detail.
     *
     * @param isbn13
     *            the isbn13
     * @param model
     *            the model
     * @param session
     *            the session
     * @return the string
     */
    @RequestMapping(value = "/detail/{isbn13}", method = RequestMethod.GET)
    public String detail(@PathVariable(value = "isbn13") String isbn13, Model model, HttpSession session) {

	model.addAttribute("book", bookService.rechercher(isbn13));

	return "book-detail";
    }

    /**
     * Adds the book to cart.
     *
     * @param isbn13
     *            the isbn13
     * @param quantity
     *            the quantity
     * @param model
     *            the model
     * @param session
     *            the session
     * @return the string
     */
    @RequestMapping(value = "/addtocart/{isbn13}/{qty}", method = RequestMethod.GET)
    public @ResponseBody String addBookToCart(@PathVariable(value = "isbn13") String isbn13, @PathVariable(value = "qty") String quantity,
	    Model model, HttpSession session) {

	addCartArticle(session, new CartArticle(bookService.rechercher(isbn13), quantity));

	return getCartQuantity(session);
    }

    /**
     * Index.
     *
     * @param model
     *            the model
     * @param bookSearchModel
     *            the book search model
     * @param bindingResult
     *            the binding result
     * @return the string
     */
    @RequestMapping(value = "/search.html", method = RequestMethod.GET)
    public String index(Model model, @Validated BookSearchModel bookSearchModel, BindingResult bindingResult) {

	List<BookDto> books = new ArrayList<BookDto>();

	if (!bindingResult.hasErrors()) {
	    books = bookService.rechercherAvecCriteres(new BookCriteria.BookCriteriaBuilder().withTitle(bookSearchModel.getTitle())
		    .withAuthorId(bookSearchModel.getAuthorId()).withEditor(bookSearchModel.getEditor())
		    .withBookId(bookSearchModel.getBookId()).withMaxPrice(bookSearchModel.getMaxPrice()).build());
	} else {
	    String errors = BindingResultUtils.getBindingMessages(bindingResult);
	    model.addAttribute("errors", errors);
	}

	model.addAttribute("books", books);
	model.addAttribute("authors", authorService.rechercher());
	model.addAttribute("bookSearchModel", bookSearchModel);

	return "books";
    }

}
