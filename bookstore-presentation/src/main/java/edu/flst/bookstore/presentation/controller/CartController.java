package edu.flst.bookstore.presentation.controller;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import edu.flst.bookstore.presentation.model.CartArticle;
import edu.flst.bookstore.service.BookService;

/**
 * Created by Dimitri on 19/10/14.
 */
@Controller
@RequestMapping("/cart")
public class CartController extends AbstractController {

    /**
     * The Constant LOGGER.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(CartController.class);

    /** The book service. */
    @Autowired
    private BookService bookService;

    /**
     * Cart.
     *
     * @param model
     *            the model
     * @return the string
     */
    @RequestMapping(value = "/cart.html", method = RequestMethod.GET)
    public String cart(Model model) {

	return "cart";
    }

    /**
     * Delete article.
     *
     * @param session
     *            the session
     * @param isbn13
     *            the isbn13
     * @return the string
     */
    @RequestMapping(value = "/delete/{isbn13}", method = RequestMethod.GET)
    public String deleteArticle(HttpSession session, @PathVariable(value = "isbn13") String isbn13) {

	deleteCartArticle(session, new CartArticle(bookService.rechercher(isbn13)));

	return "redirect:/cart/cart.html";
    }

    /**
     * Update article.
     *
     * @param isbn13
     *            the isbn13
     * @param quantity
     *            the quantity
     * @param model
     *            the model
     * @param session
     *            the session
     * @return the string
     */
    @RequestMapping(value = "/update/{isbn13}/{qty}", method = RequestMethod.GET)
    public @ResponseBody String updateArticle(@PathVariable(value = "isbn13") String isbn13, @PathVariable(value = "qty") String quantity,
	    Model model, HttpSession session) {

	Integer q;
	try {
	    q = Integer.parseInt(quantity);
	} catch (NumberFormatException exception) {
	    q = 1;
	}

	if (q.equals(0)) {
	    deleteCartArticle(session, new CartArticle(bookService.rechercher(isbn13)));
	    return "refresh";
	}

	q = q > 0 && q < 100 ? q : 1;

	updateCartArticle(session, new CartArticle(bookService.rechercher(isbn13)), q);

	return "ok";
    }

    /**
     * Gets the count.
     *
     * @param model
     *            the model
     * @param session
     *            the session
     * @return the count
     */
    @RequestMapping(value = "/count.html", method = RequestMethod.GET)
    public @ResponseBody String getCount(Model model, HttpSession session) {
	return getCartQuantity(session);
    }

}
