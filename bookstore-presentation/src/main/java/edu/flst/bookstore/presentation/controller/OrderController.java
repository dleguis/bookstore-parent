package edu.flst.bookstore.presentation.controller;

import java.util.HashSet;
import java.util.Set;

import javax.servlet.http.HttpSession;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import edu.flst.bookstore.presentation.model.CartArticle;
import edu.flst.bookstore.service.OrderService;
import edu.flst.bookstore.service.UserService;
import edu.flst.bookstore.transverse.dto.OrderDto;
import edu.flst.bookstore.transverse.dto.OrderLineDto;
import edu.flst.bookstore.transverse.dto.UserDto;

/**
 * Created by Dimitri on 19/10/14.
 */
@Controller
@RequestMapping("/order")
public class OrderController extends AbstractController {

    /**
     * The Constant LOGGER.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(OrderController.class);

    /** The user service. */
    @Autowired
    private UserService userService;

    /** The order service. */
    @Autowired
    private OrderService orderService;

    /**
     * Recap.
     *
     * @param model the model
     * @return the string
     */
    @RequestMapping(value = "/recap.html", method = RequestMethod.GET)
    public String recap(Model model) {
	UserDto user = userService.rechercher(getUserProfil().getUsername());
	model.addAttribute("userAddress", user.getMailingAddress());
	return "recap";
    }

    /**
     * Do order.
     *
     * @param model the model
     * @param session the session
     * @return the string
     */
    @RequestMapping(value = "/doorder.html", method = RequestMethod.POST)
    public String doOrder(Model model, HttpSession session) {

	UserDto user = userService.rechercher(getUserProfil().getUsername());

	OrderDto order = new OrderDto();
	// Adresse utilisateur
	order.setMailingAddress(user.getMailingAddress());
	// Date du jour
	order.setOrderDate(DateTime.now().toDate());
	// Ajout du User
	order.setUser(user);

	Set<OrderLineDto> orderLines = new HashSet<>();

	for (CartArticle article : getCart(session).getArticles()) {
	    OrderLineDto orderLine = new OrderLineDto();
	    orderLine.setBook(article.getBook());
	    orderLine.setBookId(article.getBook().getIsbn13());
	    orderLine.setQuantity(article.getQuantity());

	    orderLines.add(orderLine);
	}

	order.setOrderLines(orderLines);

	// Enregistrement de la commande
	orderService.enregistrer(order);
	// Vidage du panier
	clearCart(session);
	return "redirect:/order/ordersuccess.html";
    }

    /**
     * Order success.
     *
     * @param model the model
     * @param session the session
     * @return the string
     */
    @RequestMapping(value = "/ordersuccess.html", method = RequestMethod.GET)
    public String orderSuccess(Model model, HttpSession session) {
	return "order-success";
    }

}
