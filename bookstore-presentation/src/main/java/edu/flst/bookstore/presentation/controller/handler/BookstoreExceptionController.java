package edu.flst.bookstore.presentation.controller.handler;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

import edu.flst.bookstore.transverse.exception.BookstoreFonctionnelleException;

/**
 * Created by Dimitri on 07/12/2014.
 */
@ControllerAdvice
public class BookstoreExceptionController {

    /**
     * Erreur fonctionnelle.
     *
     * @param exception the exception
     * @return the model and view
     */
    @ExceptionHandler(BookstoreFonctionnelleException.class)
    public ModelAndView erreurFonctionnelle(final BookstoreFonctionnelleException exception) {
	ModelAndView modelAndView = new ModelAndView("error");
	modelAndView.addObject("errorCode", exception.getExceptionCause().name());
	modelAndView.addObject("errorMessage", exception.getMessage());
	return modelAndView;
    }
}
