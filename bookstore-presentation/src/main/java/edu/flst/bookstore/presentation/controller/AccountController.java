package edu.flst.bookstore.presentation.controller;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import edu.flst.bookstore.presentation.model.UserModificationModel;
import edu.flst.bookstore.service.UserService;
import edu.flst.bookstore.transverse.dto.MailingAddressDto;
import edu.flst.bookstore.transverse.dto.UserDto;
import edu.flst.bookstore.transverse.exception.BookstoreFonctionnelleException;
import edu.flst.bookstore.transverse.exception.code.BookstoreFonctionnelleExceptionCode;
import edu.flst.bookstore.transverse.utils.binding.BindingResultUtils;

/**
 * Created by Dimitri on 19/10/14.
 */
@Controller
public class AccountController extends AbstractController {

    /**
     * The Constant LOGGER.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(AccountController.class);

    /**
     * The user service.
     */
    @Autowired
    private UserService userService;

    /**
     * Display account.
     *
     * @param model
     *            the model
     * @param userLogin
     *            the user login
     * @return the string
     */
    @RequestMapping(value = "/profil/{userLogin}", method = RequestMethod.GET)
    public String displayAccount(Model model, @PathVariable("userLogin") String userLogin) {

	UserDto userDto = userService.rechercher(userLogin, true);
	model.addAttribute("user", userDto);
	model.addAttribute("userModel", new UserModificationModel(userDto));
	model.addAttribute("errors", "");
	return "account";
    }

    /**
     * Save account.
     *
     * @param userLogin            the user login
     * @param model            the model
     * @param userModel            the user model
     * @param bindingResult            the binding result
     * @return the string
     */
    @RequestMapping(value = "/profil/{userLogin}", method = RequestMethod.POST)
    // @Secured({"ROLE_MODIFY_PASSWORD", "ROLE_MODIFY_ALL_PASSWORDS"})
    public String saveAccount(@PathVariable("userLogin") String userLogin, Model model,
	    @ModelAttribute("userModel") @Validated UserModificationModel userModel, BindingResult bindingResult) {

	if (!userModel.getLogin().equals(getUserProfil().getUsername())) {
	    throw new BookstoreFonctionnelleException(BookstoreFonctionnelleExceptionCode.ERR_USER_003);
	}

	String errors = "";

	if (!bindingResult.hasErrors()
		&& (getUserProfil().getUsername().equals(userModel.getLogin()) || getUserProfil().hasAuthority("ROLE_MODIFY_ALL_PASSWORDS"))) {
	    UserDto user = userService.rechercher(userModel.getLogin(), true);

	    user.setLogin(userModel.getLogin());
	    LOGGER.debug("Comparaison de {} et {}", user.getPassword(), userModel.getPassword());
	    if (user.getPassword().equals(userModel.getPassword())) {
		if (StringUtils.isNotBlank(userModel.getNewPassword())) {
		    user.setPassword(userModel.getNewPassword());
		} else {
		    user.setPassword(userModel.getPassword());
		}
	    } else {
		throw new BookstoreFonctionnelleException(BookstoreFonctionnelleExceptionCode.ERR_USER_004);
	    }

	    user.setEmail(userModel.getEmail());

	    MailingAddressDto address = new MailingAddressDto();
	    address.setId(userModel.getAddressId());
	    address.setLine1(userModel.getLine1());
	    address.setLine2(userModel.getLine2());
	    address.setLine3(userModel.getLine3());
	    address.setCity(userModel.getCity());
	    address.setZip(userModel.getZip());

	    user.setMailingAddress(address);

	    userService.enregistrer(user);
	} else {
	    errors = BindingResultUtils.getBindingMessages(bindingResult);
	}

	model.addAttribute("errors", errors);

	UserDto userDto = userService.rechercher(userLogin, true);
	model.addAttribute("user", userDto);
	model.addAttribute("userModel", new UserModificationModel(userDto));

	return "account";
    }

}
