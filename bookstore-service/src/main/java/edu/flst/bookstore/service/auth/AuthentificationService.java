package edu.flst.bookstore.service.auth;

import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * Created by Dimitri on 02/11/14.
 */
public interface AuthentificationService extends UserDetailsService {
}
