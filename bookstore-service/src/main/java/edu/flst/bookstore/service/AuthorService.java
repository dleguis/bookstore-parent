package edu.flst.bookstore.service;

import java.util.List;

import edu.flst.bookstore.transverse.dto.AuthorDto;

/**
 * Projet Bookstore (FST)
 * Créé par Dimitri le 16/11/14.
 */
public interface AuthorService {

    /**
     * Rechercher list.
     *
     * @return the list
     */
    List<AuthorDto> rechercher();

    /**
     * Rechercher author dto.
     *
     * @param id the id
     * @return the author dto
     */
    AuthorDto rechercher(Integer id);

}
