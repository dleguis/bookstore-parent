package edu.flst.bookstore.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import edu.flst.bookstore.domaine.bo.Author;
import edu.flst.bookstore.service.AuthorService;
import edu.flst.bookstore.transverse.dto.AuthorDto;
import edu.flst.bookstore.transverse.exception.BookstoreFonctionnelleException;
import edu.flst.bookstore.transverse.exception.code.BookstoreFonctionnelleExceptionCode;
import edu.flst.bookstore.transverse.utils.mapper.MapperUtils;

/**
 * Projet Bookstore (FST)
 * Créé par Dimitri le 16/11/14.
 */
@Service
public class AuthorServiceImpl extends AbstractServiceImpl implements AuthorService {

    /**
     * The Constant LOGGER.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(AuthorServiceImpl.class);

    /*
     * (non-Javadoc)
     *
     * @see edu.flst.bookstore.service.AuthorService#rechercher()
     */
    @Override
    public List<AuthorDto> rechercher() {
	return MapperUtils.map(mapperService, authorRepository.findAll(), AuthorDto.class);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * edu.flst.bookstore.service.AuthorService#rechercher(java.lang.Integer)
     */
    @Override
    public AuthorDto rechercher(Integer id) {

	Author author = authorRepository.findOne(id);

	if (author == null) {
	    throw new BookstoreFonctionnelleException(BookstoreFonctionnelleExceptionCode.ERR_AUTHOR_001);
	}

	return MapperUtils.map(mapperService, author, AuthorDto.class);
    }

}
