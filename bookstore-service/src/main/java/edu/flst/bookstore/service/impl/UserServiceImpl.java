package edu.flst.bookstore.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import edu.flst.bookstore.domaine.bo.MailingAddress;
import edu.flst.bookstore.domaine.bo.User;
import edu.flst.bookstore.service.UserService;
import edu.flst.bookstore.transverse.dto.UserDto;
import edu.flst.bookstore.transverse.exception.BookstoreFonctionnelleException;
import edu.flst.bookstore.transverse.exception.code.BookstoreFonctionnelleExceptionCode;
import edu.flst.bookstore.transverse.utils.mapper.MapperUtils;

/**
 * Projet Bookstore (FST) Créé par Dimitri le 02/11/14.
 */
@Service
public class UserServiceImpl extends AbstractServiceImpl implements UserService {

    /*
     * (non-Javadoc)
     *
     * @see edu.flst.bookstore.service.UserService#rechercher()
     */
    @Override
    public List<UserDto> rechercher() {
	return MapperUtils.map(mapperService, userRepository.findAll(), UserDto.class);
    }

    /*
     * (non-Javadoc)
     *
     * @see edu.flst.bookstore.service.UserService#rechercher(java.lang.String)
     */
    @Override
    public UserDto rechercher(String login) throws BookstoreFonctionnelleException {
	return rechercher(login, false);
    }

    /*
     * (non-Javadoc)
     *
     * @see edu.flst.bookstore.service.UserService#rechercher(java.lang.String,
     * java.lang.Boolean)
     */
    @Override
    public UserDto rechercher(String login, Boolean deepSearch) throws BookstoreFonctionnelleException {
	User user = deepSearch ? userRepository.findOneDeep(login) : userRepository.findOne(login);

	if (user == null) {
	    throw new BookstoreFonctionnelleException(BookstoreFonctionnelleExceptionCode.ERR_USER_002);
	}

	return MapperUtils.map(mapperService, user, UserDto.class);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * edu.flst.bookstore.service.UserService#creer(edu.flst.bookstore.transverse
     * .dto.UserDto)
     */
    @Override
    public void creer(UserDto user) throws BookstoreFonctionnelleException {
	// Utilisateur existant ?
	if (userRepository.exists(user.getLogin())) {
	    throw new BookstoreFonctionnelleException(BookstoreFonctionnelleExceptionCode.ERR_USER_001);
	}

	enregistrer(user);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * edu.flst.bookstore.service.UserService#enregistrer(edu.flst.bookstore
     * .transverse.dto.UserDto)
     */
    @Override
    public void enregistrer(UserDto user) {
	User u = MapperUtils.map(mapperService, user, User.class);

	MailingAddress m = mailingAddressRepository.save(u.getMailingAddress());

	u.setMailingAddress(m);

	userRepository.save(u);
    }
}
