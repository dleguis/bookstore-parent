package edu.flst.bookstore.service;

import java.util.List;

import edu.flst.bookstore.transverse.dto.OrderDto;

/**
 * Projet Bookstore (FST) Créé par Dimitri le 16/11/14.
 */
public interface OrderService {

    /**
     * Rechercher.
     *
     * @return the list
     */
    List<OrderDto> rechercher();

    /**
     * Rechercher.
     *
     * @param id the id
     * @return the order dto
     */
    OrderDto rechercher(Integer id);

    /**
     * Enregistrer.
     *
     * @param order the order
     */
    void enregistrer(OrderDto order);
}
