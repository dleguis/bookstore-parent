package edu.flst.bookstore.service.auth.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import edu.flst.bookstore.domaine.bo.User;
import edu.flst.bookstore.domaine.repository.UserRepository;
import edu.flst.bookstore.service.auth.AuthentificationService;
import edu.flst.bookstore.transverse.dto.Utilisateur;

/**
 * Created by Dimitri on 02/11/14.
 */
@Service
public class AuthentificationServiceImpl implements AuthentificationService {

    /**
     * The user repository.
     */
    @Autowired
    private UserRepository userRepository;

    /*
     * (non-Javadoc)
     *
     * @see org.springframework.security.core.userdetails.UserDetailsService#
     * loadUserByUsername(java.lang.String)
     */
    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {

	User user = userRepository.findOne(s);

	if (user == null) {
	    throw new UsernameNotFoundException(String.format("Utilisateur {} introuvable", s));
	}

	List<GrantedAuthority> authorities = new ArrayList<>();
	authorities.add(new SimpleGrantedAuthority("ROLE_USER"));

	return new Utilisateur(user.getLogin(), user.getPassword(), authorities);
    }
}
