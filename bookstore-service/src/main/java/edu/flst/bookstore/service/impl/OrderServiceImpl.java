package edu.flst.bookstore.service.impl;

import java.util.Collection;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import edu.flst.bookstore.domaine.bo.Order;
import edu.flst.bookstore.domaine.bo.OrderLine;
import edu.flst.bookstore.service.OrderService;
import edu.flst.bookstore.transverse.dto.OrderDto;
import edu.flst.bookstore.transverse.exception.BookstoreFonctionnelleException;
import edu.flst.bookstore.transverse.exception.code.BookstoreFonctionnelleExceptionCode;
import edu.flst.bookstore.transverse.utils.mapper.MapperUtils;

/**
 * Projet Bookstore (FST) Créé par Dimitri le 16/11/14.
 */
@Service
public class OrderServiceImpl extends AbstractServiceImpl implements OrderService {

    /**
     * The Constant LOGGER.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(OrderServiceImpl.class);

    /*
     * (non-Javadoc)
     * 
     * @see edu.flst.bookstore.service.OrderService#rechercher()
     */
    @Override
    public List<OrderDto> rechercher() {
	return MapperUtils.map(mapperService, orderRepository.findAll(), OrderDto.class);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * edu.flst.bookstore.service.OrderService#rechercher(java.lang.Integer)
     */
    @Override
    public OrderDto rechercher(Integer id) {

	Order order = orderRepository.findOne(id);

	if (order == null) {
	    throw new BookstoreFonctionnelleException(BookstoreFonctionnelleExceptionCode.ERR_ORDER_001);
	}

	return MapperUtils.map(mapperService, order, OrderDto.class);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * edu.flst.bookstore.service.OrderService#enregistrer(edu.flst.bookstore
     * .transverse.dto.OrderDto)
     */
    @Override
    public void enregistrer(OrderDto order) {
	if (order.getId() != null) {
	    throw new BookstoreFonctionnelleException(BookstoreFonctionnelleExceptionCode.ERR_ORDER_002);
	}
	Order savedOrder = orderRepository.save(MapperUtils.map(mapperService, order, Order.class));

	Collection<OrderLine> orderLines = MapperUtils.map(mapperService, order.getOrderLines(), OrderLine.class);

	for (OrderLine myOrderLine : orderLines) {
	    LOGGER.debug("Assignation de l'id {} pour l'artilce {}", savedOrder.getId(), myOrderLine.getBookId());
	    myOrderLine.setOrderId(savedOrder.getId());
	}

	orderLineRepository.save(orderLines);
	LOGGER.debug("BLAAA");
    }
}
