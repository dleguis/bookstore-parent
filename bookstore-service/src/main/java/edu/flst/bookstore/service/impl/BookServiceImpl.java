package edu.flst.bookstore.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import edu.flst.bookstore.domaine.bo.Book;
import edu.flst.bookstore.service.BookService;
import edu.flst.bookstore.transverse.builder.BookCriteria;
import edu.flst.bookstore.transverse.dto.BookDto;
import edu.flst.bookstore.transverse.exception.BookstoreFonctionnelleException;
import edu.flst.bookstore.transverse.exception.code.BookstoreFonctionnelleExceptionCode;
import edu.flst.bookstore.transverse.utils.mapper.MapperUtils;

/**
 * Projet Bookstore (FST) Créé par Dimitri le 16/11/14.
 */
@Service
public class BookServiceImpl extends AbstractServiceImpl implements BookService {

    /*
     * (non-Javadoc)
     *
     * @see edu.flst.bookstore.service.BookService#rechercher()
     */
    @Override
    public List<BookDto> rechercher() {
	return MapperUtils.map(mapperService, bookRepository.findAll(), BookDto.class);
    }

    /*
     * (non-Javadoc)
     *
     * @see edu.flst.bookstore.service.BookService#rechercher(java.lang.String)
     */
    @Override
    public BookDto rechercher(String isbn13) {
	Book book = bookRepository.findOne(isbn13);

	if (book == null) {
	    throw new BookstoreFonctionnelleException(BookstoreFonctionnelleExceptionCode.ERR_BOOK_001);
	}

	return MapperUtils.map(mapperService, book, BookDto.class);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * edu.flst.bookstore.service.BookService#rechercherAvecCriteres(edu.flst
     * .bookstore.transverse.builder.BookCriteria)
     */
    @Override
    public List<BookDto> rechercherAvecCriteres(BookCriteria bookCriteria) {
	return MapperUtils.map(mapperService, bookRepository.findByCriteria(bookCriteria), BookDto.class);
    }
}
