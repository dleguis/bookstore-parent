package edu.flst.bookstore.service.impl;

import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import edu.flst.bookstore.domaine.repository.AuthorRepository;
import edu.flst.bookstore.domaine.repository.BookRepository;
import edu.flst.bookstore.domaine.repository.MailingAddressRepository;
import edu.flst.bookstore.domaine.repository.OrderLineRepository;
import edu.flst.bookstore.domaine.repository.OrderRepository;
import edu.flst.bookstore.domaine.repository.UserRepository;

/**
 * Projet Bookstore (FST)
 * Créé par Dimitri le 16/11/14.
 */
@Service
@Transactional
public abstract class AbstractServiceImpl {

    /**
     * The user repository.
     */
    @Autowired
    protected UserRepository userRepository;

    /**
     * The order repository.
     */
    @Autowired
    protected OrderRepository orderRepository;

    /**
     * The order line repository.
     */
    @Autowired
    protected OrderLineRepository orderLineRepository;

    /**
     * The book repository.
     */
    @Autowired
    protected BookRepository bookRepository;

    /** The author repository. */
    @Autowired
    protected AuthorRepository authorRepository;

    /**
     * The mailing address repository.
     */
    @Autowired
    protected MailingAddressRepository mailingAddressRepository;

    /** The mapper service. */
    @Autowired
    protected Mapper mapperService;

}
