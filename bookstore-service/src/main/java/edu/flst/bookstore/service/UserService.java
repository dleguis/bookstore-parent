package edu.flst.bookstore.service;

import java.util.List;

import edu.flst.bookstore.transverse.dto.UserDto;
import edu.flst.bookstore.transverse.exception.BookstoreFonctionnelleException;

/**
 * Projet Bookstore (FST) Créé par Dimitri le 02/11/14.
 */
public interface UserService {

    /**
     * Rechercher.
     *
     * @return the list
     */
    List<UserDto> rechercher();

    /**
     * Rechercher.
     *
     * @param login the login
     * @return the user dto
     * @throws BookstoreFonctionnelleException the bookstore fonctionnelle
     *                                         exception
     */
    UserDto rechercher(String login) throws BookstoreFonctionnelleException;

    /**
     * Rechercher.
     *
     * @param login the login
     * @param deepSearch the deep search
     * @return the user dto
     * @throws BookstoreFonctionnelleException the bookstore fonctionnelle
     *             exception
     */
    UserDto rechercher(String login, Boolean deepSearch) throws BookstoreFonctionnelleException;

    /**
     * Creer.
     *
     * @param user the user
     * @throws BookstoreFonctionnelleException the bookstore fonctionnelle
     *             exception
     */
    void creer(UserDto user) throws BookstoreFonctionnelleException;

    /**
     * Enregistrer.
     *
     * @param user the user
     */
    void enregistrer(UserDto user);
}
