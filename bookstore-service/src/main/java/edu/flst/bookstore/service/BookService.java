package edu.flst.bookstore.service;

import java.util.List;

import edu.flst.bookstore.transverse.builder.BookCriteria;
import edu.flst.bookstore.transverse.dto.BookDto;

/**
 * Projet Bookstore (FST) Créé par Dimitri le 16/11/14.
 */
public interface BookService {

    /**
     * Rechercher.
     *
     * @return the list
     */
    List<BookDto> rechercher();

    /**
     * Rechercher.
     *
     * @param isbn13 the isbn13
     * @return the book dto
     */
    BookDto rechercher(String isbn13);

    /**
     * Rechercher avec criteres.
     *
     * @param bookCriteria the book criteria
     * @return the list
     */
    List<BookDto> rechercherAvecCriteres(BookCriteria bookCriteria);
}
