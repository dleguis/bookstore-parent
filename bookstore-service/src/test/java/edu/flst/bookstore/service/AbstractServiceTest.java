package edu.flst.bookstore.service;

import org.dozer.Mapper;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import edu.flst.bookstore.domaine.repository.BookRepository;
import edu.flst.bookstore.domaine.repository.OrderLineRepository;
import edu.flst.bookstore.domaine.repository.OrderRepository;
import edu.flst.bookstore.domaine.repository.UserRepository;

/**
 * Created by Dimitri on 14/01/2015.
 */
@ContextConfiguration({ "classpath*:config/bookstore-service-test-context.xml", "classpath:config/bookstore-domaine-datasource.xml" })
@RunWith(SpringJUnit4ClassRunner.class)
public abstract class AbstractServiceTest {

    /**
     * The user repository.
     */
    @Autowired
    protected UserRepository userRepository;

    /** The order repository. */
    @Autowired
    protected OrderRepository orderRepository;

    /** The order line repository. */
    @Autowired
    protected OrderLineRepository orderLineRepository;

    /** The book repository. */
    @Autowired
    protected BookRepository bookRepository;

    /** The mapper service. */
    @Autowired
    protected Mapper mapperService;

    /** The book service. */
    @Autowired
    protected BookService bookService;

    /** The author service. */
    @Autowired
    protected AuthorService authorService;

    /** The order service. */
    @Autowired
    protected OrderService orderService;

    /** The user service. */
    @Autowired
    protected UserService userService;
}
