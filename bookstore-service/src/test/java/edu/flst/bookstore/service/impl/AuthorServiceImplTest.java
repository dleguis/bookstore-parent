package edu.flst.bookstore.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.junit.Test;

import edu.flst.bookstore.service.AbstractServiceTest;
import edu.flst.bookstore.transverse.dto.AuthorDto;
import edu.flst.bookstore.transverse.exception.BookstoreFonctionnelleException;

/**
 * Created by Dimitri on 14/01/2015.
 */
public class AuthorServiceImplTest extends AbstractServiceTest {

    /**
     * Rechercher test.
     */
    @Test
    public void rechercherTest() {
	List<AuthorDto> authors = authorService.rechercher();
	assertNotNull(authors);
	assertEquals(6, CollectionUtils.size(authors));
    }

    /**
     * Rechercher one test.
     */
    @Test
    public void rechercherOneTest() {
	assertNotNull(orderService.rechercher(1));
    }

    /**
     * Rechercher one inexistant test.
     */
    @Test(expected = BookstoreFonctionnelleException.class)
    public void rechercherOneInexistantTest() {
	assertNotNull(orderService.rechercher(999));
    }

}
