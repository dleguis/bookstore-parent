package edu.flst.bookstore.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.joda.time.DateTime;
import org.junit.Test;
import org.springframework.transaction.annotation.Transactional;

import edu.flst.bookstore.service.AbstractServiceTest;
import edu.flst.bookstore.transverse.dto.BookDto;
import edu.flst.bookstore.transverse.dto.OrderDto;
import edu.flst.bookstore.transverse.dto.OrderLineDto;
import edu.flst.bookstore.transverse.dto.UserDto;
import edu.flst.bookstore.transverse.exception.BookstoreFonctionnelleException;

/**
 * Created by Dimitri on 14/01/2015.
 */
public class OrderServiceImplTest extends AbstractServiceTest {

    /**
     * Rechercher test.
     */
    @Test
    public void rechercherTest() {
	List<OrderDto> orders = orderService.rechercher();
	assertNotNull(orders);
	assertEquals(2, CollectionUtils.size(orders));
    }

    /**
     * Rechercher one test.
     */
    @Test
    public void rechercherOneTest() {
	assertNotNull(orderService.rechercher(1));
    }

    /**
     * Rechercher one inexistant test.
     */
    @Test(expected = BookstoreFonctionnelleException.class)
    public void rechercherOneInexistantTest() {
	assertNotNull(orderService.rechercher(999));
    }

    /**
     * Enregistrer test.
     */
    @Test
    @Transactional
    public void enregistrerTest() {

	UserDto u = userService.rechercher("yhovart");
	OrderDto o = new OrderDto();

	o.setMailingAddress(u.getMailingAddress());
	o.setUser(u);
	o.setOrderDate(DateTime.now().toDate());

	BookDto b = bookService.rechercher("978-1430219569");

	OrderLineDto ol = new OrderLineDto();
	ol.setBook(b);
	ol.setBookId(b.getIsbn13());
	ol.setQuantity(12);

	List<OrderLineDto> orderLines = new ArrayList<>();
	orderLines.add(ol);
	o.setOrderLines(orderLines);

	orderService.enregistrer(o);
    }
}
