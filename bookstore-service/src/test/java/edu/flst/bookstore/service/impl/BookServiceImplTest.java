package edu.flst.bookstore.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.junit.Test;

import edu.flst.bookstore.service.AbstractServiceTest;
import edu.flst.bookstore.transverse.builder.BookCriteria;
import edu.flst.bookstore.transverse.dto.BookDto;
import edu.flst.bookstore.transverse.exception.BookstoreFonctionnelleException;

/**
 * Created by Dimitri on 14/01/2015.
 */
public class BookServiceImplTest extends AbstractServiceTest {

    /**
     * Find by criteria test.
     */
    @Test
    public void findByCriteriaTest() {
	List<BookDto> books = bookService.rechercherAvecCriteres(new BookCriteria.BookCriteriaBuilder().withBookId("978-1430219569")
	        .withEditor("APress").withAuthorId(1).withMaxPrice(40.85F).build());

	assertTrue(CollectionUtils.isNotEmpty(books));
    }

    /**
     * Rechercher test.
     */
    @Test
    public void rechercherTest() {
	assertEquals(7, CollectionUtils.size(bookService.rechercher()));
    }

    /**
     * Rechercher one test.
     */
    @Test
    public void rechercherOneTest() {
	assertNotNull(bookService.rechercher("978-1430219569"));
    }

    /**
     * Rechercher one inexistant test.
     */
    @Test(expected = BookstoreFonctionnelleException.class)
    public void rechercherOneInexistantTest() {
	assertNotNull(bookService.rechercher("978-INEXISTANT"));
    }
}
