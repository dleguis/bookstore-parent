package edu.flst.bookstore.service.auth.impl;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import edu.flst.bookstore.service.AbstractServiceTest;
import edu.flst.bookstore.service.auth.AuthentificationService;

/**
 * Created by Dimitri on 08/11/14.
 */
public class AuthentificationServiceImplTest extends AbstractServiceTest {

    /**
     * The authentification service.
     */
    @Autowired
    AuthentificationService authentificationService;

    /**
     * Test load by username.
     *
     * @throws Exception the exception
     */
    @Test
    public void testLoadByUsername() throws Exception {
	assertNotNull(authentificationService.loadUserByUsername("yhovart"));
    }
}
