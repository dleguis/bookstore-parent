package edu.flst.bookstore.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.junit.Test;
import org.springframework.transaction.annotation.Transactional;

import edu.flst.bookstore.service.AbstractServiceTest;
import edu.flst.bookstore.transverse.dto.MailingAddressDto;
import edu.flst.bookstore.transverse.dto.UserDto;
import edu.flst.bookstore.transverse.exception.BookstoreFonctionnelleException;

/**
 * Created by Dimitri on 14/01/2015.
 */
public class UserServiceImplTest extends AbstractServiceTest {

    /**
     * Rechercher test.
     */
    @Test
    public void rechercherTest() {
	List<UserDto> users = userService.rechercher();
	assertNotNull(users);
	assertEquals(1, CollectionUtils.size(users));
    }

    /**
     * Rechercher one test.
     */
    @Test
    public void rechercherOneTest() {
	assertNotNull(userService.rechercher("yhovart"));
    }

    /**
     * Rechercher one inexistant test.
     */
    @Test(expected = BookstoreFonctionnelleException.class)
    public void rechercherOneInexistantTest() {
	assertNotNull(userService.rechercher("yhovart-inexistant"));
    }

    /**
     * Rechercher one deep test.
     */
    @Test
    public void rechercherOneDeepTest() {
	assertNotNull(userService.rechercher("yhovart", true));
    }

    /**
     * Creer test.
     */
    @Test
    @Transactional
    public void creerTest() {

	UserDto u = new UserDto();

	u.setEmail("toto@oto.fr");
	u.setLogin("totoJUnit");
	u.setPassword("totopassword");

	MailingAddressDto m = new MailingAddressDto();
	m.setLine1("3 rue des bla");
	m.setLine2("apt 2");
	m.setLine3("étage 7");
	m.setCity("TotoLand");
	m.setZip("99999");

	u.setMailingAddress(m);

	userService.creer(u);
    }

    /**
     * Creer existant test.
     */
    @Test(expected = BookstoreFonctionnelleException.class)
    @Transactional
    public void creerExistantTest() {

	UserDto u = new UserDto();

	u.setEmail("toto@oto.fr");
	u.setLogin("yhovart");
	u.setPassword("totopassword");

	MailingAddressDto m = new MailingAddressDto();
	m.setLine1("3 rue des bla");
	m.setLine2("apt 2");
	m.setLine3("étage 7");
	m.setCity("TotoLand");
	m.setZip("99999");

	u.setMailingAddress(m);

	userService.creer(u);
    }
}
