package edu.flst.bookstore.domaine.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import edu.flst.bookstore.domaine.bo.OrderLine;
import edu.flst.bookstore.domaine.bo.OrderLinesPK;

/**
 * Created by Dimitri on 19/10/14.
 */
public interface OrderLineRepository extends JpaRepository<OrderLine, OrderLinesPK> {

}
