package edu.flst.bookstore.domaine.bo;

import java.util.Collection;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Created by Dimitri on 08/11/14.
 */
@Entity
@Table(name = "BOOKS")
public class Book {

    /**
     * The isbn13.
     */
    private String isbn13;

    /** The title. */
    private String title;

    /** The editor. */
    private String editor;

    /** The unit price. */
    private Float unitPrice;

    /** The author. */
    private Author author;

    /** The order lines. */
    private Collection<OrderLine> orderLines;

    /**
     * Gets isbn 13.
     *
     * @return the isbn 13
     */
    @Id
    @Column(name = "ISBN13")
    public String getIsbn13() {
	return isbn13;
    }

    /**
     * Sets isbn 13.
     *
     * @param isbn13
     *            the isbn 13
     */
    public void setIsbn13(String isbn13) {
	this.isbn13 = isbn13;
    }

    /**
     * Gets title.
     *
     * @return the title
     */
    @Basic
    @Column(name = "TITLE")
    public String getTitle() {
	return title;
    }

    /**
     * Sets title.
     *
     * @param title
     *            the title
     */
    public void setTitle(String title) {
	this.title = title;
    }

    /**
     * Gets editor.
     *
     * @return the editor
     */
    @Basic
    @Column(name = "EDITOR")
    public String getEditor() {
	return editor;
    }

    /**
     * Sets editor.
     *
     * @param editor
     *            the editor
     */
    public void setEditor(String editor) {
	this.editor = editor;
    }

    /**
     * Gets unit price.
     *
     * @return the unit price
     */
    @Basic
    @Column(name = "UNIT_PRICE")
    public Float getUnitPrice() {
	return unitPrice;
    }

    /**
     * Sets unit price.
     *
     * @param unitPrice
     *            the unit price
     */
    public void setUnitPrice(Float unitPrice) {
	this.unitPrice = unitPrice;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object o) {
	if (this == o) {
	    return true;
	}
	if (o == null || getClass() != o.getClass()) {
	    return false;
	}

	Book book = (Book) o;

	if (editor != null ? !editor.equals(book.editor) : book.editor != null) {
	    return false;
	}
	if (isbn13 != null ? !isbn13.equals(book.isbn13) : book.isbn13 != null) {
	    return false;
	}
	if (title != null ? !title.equals(book.title) : book.title != null) {
	    return false;
	}
	if (unitPrice != null ? !unitPrice.equals(book.unitPrice) : book.unitPrice != null) {
	    return false;
	}

	return true;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
	int result = isbn13 != null ? isbn13.hashCode() : 0;
	result = 31 * result + (title != null ? title.hashCode() : 0);
	result = 31 * result + (editor != null ? editor.hashCode() : 0);
	result = 31 * result + (unitPrice != null ? unitPrice.hashCode() : 0);
	return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
	return "Book{" + "isbn13='" + isbn13 + '\'' + ", title='" + title + '\'' + ", editor='" + editor + '\'' + ", unitPrice="
	        + unitPrice + ", author=" + author + '}';
    }

    /**
     * Gets author.
     *
     * @return the author
     */
    @ManyToOne
    @JoinColumn(name = "AUTHOR_ID", referencedColumnName = "ID")
    public Author getAuthor() {
	return author;
    }

    /**
     * Sets author.
     *
     * @param authorByAuthorId
     *            the author by author id
     */
    public void setAuthor(Author authorByAuthorId) {
	this.author = authorByAuthorId;
    }

    /**
     * Gets order lines.
     *
     * @return the order lines
     */
    @OneToMany(mappedBy = "book")
    public Collection<OrderLine> getOrderLines() {
	return orderLines;
    }

    /**
     * Sets order lines.
     *
     * @param orderLinesesByIsbn13
     *            the order lineses by isbn 13
     */
    public void setOrderLines(Collection<OrderLine> orderLinesesByIsbn13) {
	this.orderLines = orderLinesesByIsbn13;
    }
}
