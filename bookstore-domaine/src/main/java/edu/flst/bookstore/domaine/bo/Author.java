package edu.flst.bookstore.domaine.bo;

import java.sql.Date;
import java.util.Collection;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Created by Dimitri on 08/11/14.
 */
@Entity
@Table(name = "AUTHORS")
public class Author {
    /**
     * The Id.
     */
    private Integer id;
    /**
     * The Last name.
     */
    private String lastName;
    /**
     * The Birth date.
     */
    private Date birthDate;
    /**
     * The First name.
     */
    private String firstName;
    /**
     * The Books.
     */
    private Collection<Book> books;

    /**
     * Gets id.
     *
     * @return the id
     */
    @Id
    @Column(name = "ID")
    public Integer getId() {
	return id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(Integer id) {
	this.id = id;
    }

    /**
     * Gets last name.
     *
     * @return the last name
     */
    @Basic
    @Column(name = "LAST_NAME")
    public String getLastName() {
	return lastName;
    }

    /**
     * Sets last name.
     *
     * @param lastName the last name
     */
    public void setLastName(String lastName) {
	this.lastName = lastName;
    }

    /**
     * Gets birth date.
     *
     * @return the birth date
     */
    @Basic
    @Column(name = "BIRTH_DATE")
    public Date getBirthDate() {
	return birthDate;
    }

    /**
     * Sets birth date.
     *
     * @param birthDate the birth date
     */
    public void setBirthDate(Date birthDate) {
	this.birthDate = birthDate;
    }

    /**
     * Gets first name.
     *
     * @return the first name
     */
    @Basic
    @Column(name = "FIRST_NAME")
    public String getFirstName() {
	return firstName;
    }

    /**
     * Sets first name.
     *
     * @param firstName the first name
     */
    public void setFirstName(String firstName) {
	this.firstName = firstName;
    }

    /**
     * Equals boolean.
     *
     * @param o the o
     * @return the boolean
     */
    @Override
    public boolean equals(Object o) {
	if (this == o) {
	    return true;
	}
	if (o == null || getClass() != o.getClass()) {
	    return false;
	}

	Author author = (Author) o;

	if (id != author.id) {
	    return false;
	}
	if (birthDate != null ? !birthDate.equals(author.birthDate) : author.birthDate != null) {
	    return false;
	}
	if (firstName != null ? !firstName.equals(author.firstName) : author.firstName != null) {
	    return false;
	}
	if (lastName != null ? !lastName.equals(author.lastName) : author.lastName != null) {
	    return false;
	}

	return true;
    }

    /**
     * Hash code.
     *
     * @return the int
     */
    @Override
    public int hashCode() {
	int result = id;
	result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
	result = 31 * result + (birthDate != null ? birthDate.hashCode() : 0);
	result = 31 * result + (firstName != null ? firstName.hashCode() : 0);
	return result;
    }

    /**
     * Gets books.
     *
     * @return the books
     */
    @OneToMany(mappedBy = "author")
    public Collection<Book> getBooks() {
	return books;
    }

    /**
     * Sets books.
     *
     * @param booksesById the bookses by id
     */
    public void setBooks(Collection<Book> booksesById) {
	this.books = booksesById;
    }
}
