package edu.flst.bookstore.domaine.bo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Id;

/**
 * Created by Dimitri on 08/11/14.
 */
public class OrderLinesPK implements Serializable {

    /**
     * The orders id.
     */
    private int ordersId;

    /** The books id. */
    private String booksId;

    /**
     * Gets order id.
     *
     * @return the order id
     */
    @Column(name = "ORDERS_ID")
    @Id
    public int getOrderId() {
	return ordersId;
    }

    /**
     * Sets order id.
     *
     * @param ordersId
     *            the orders id
     */
    public void setOrderId(int ordersId) {
	this.ordersId = ordersId;
    }

    /**
     * Gets book id.
     *
     * @return the book id
     */
    @Column(name = "BOOKS_ID")
    @Id
    public String getBookId() {
	return booksId;
    }

    /**
     * Sets book id.
     *
     * @param booksId
     *            the books id
     */
    public void setBookId(String booksId) {
	this.booksId = booksId;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object o) {
	if (this == o) {
	    return true;
	}
	if (o == null || getClass() != o.getClass()) {
	    return false;
	}

	OrderLinesPK that = (OrderLinesPK) o;

	if (ordersId != that.ordersId) {
	    return false;
	}
	if (booksId != null ? !booksId.equals(that.booksId) : that.booksId != null) {
	    return false;
	}

	return true;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
	int result = ordersId;
	result = 31 * result + (booksId != null ? booksId.hashCode() : 0);
	return result;
    }
}
