package edu.flst.bookstore.domaine.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import edu.flst.bookstore.domaine.bo.Book;

/**
 * Created by Dimitri on 19/10/14.
 */
public interface BookRepository extends JpaRepository<Book, String>, BookRepositoryCustom {

}
