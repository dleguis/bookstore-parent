package edu.flst.bookstore.domaine.repository.jpa;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;

import edu.flst.bookstore.domaine.bo.Book;
import edu.flst.bookstore.domaine.repository.BookRepositoryCustom;
import edu.flst.bookstore.transverse.builder.BookCriteria;

/**
 * Created by Dimitri on 14/01/2015.
 */

public class BookRepositoryImpl implements BookRepositoryCustom {

    /**
     * The em.
     */
    @PersistenceContext
    private EntityManager em;

    /*
     * (non-Javadoc)
     * 
     * @see
     * edu.flst.bookstore.domaine.repository.BookRepositoryCustom#findByCriteria
     * (edu.flst.bookstore.transverse.builder.BookCriteria)
     */
    @Override
    public List<Book> findByCriteria(BookCriteria bookCriteria) {

	CriteriaBuilder cb = em.getCriteriaBuilder();
	CriteriaQuery<Book> query = cb.createQuery(Book.class);

	Root<Book> b = query.from(Book.class);

	List<Predicate> predicates = new ArrayList<>();

	if (!StringUtils.isBlank(bookCriteria.getTitle())) {
	    predicates.add(cb.like(b.get("title"), "%" + bookCriteria.getTitle() + "%"));
	}
	if (bookCriteria.getAuthorId() != null) {
	    predicates.add(cb.equal(b.<Integer> get("author"), bookCriteria.getAuthorId()));
	}
	if (!StringUtils.isBlank(bookCriteria.getEditor())) {
	    predicates.add(cb.like(b.get("editor"), "%" + bookCriteria.getEditor() + "%"));
	}
	if (!StringUtils.isBlank(bookCriteria.getBookId())) {
	    predicates.add(cb.like(b.get("isbn13"), "%" + bookCriteria.getBookId() + "%"));
	}
	if (bookCriteria.getMaxPrice() != null) {
	    predicates.add(cb.lessThanOrEqualTo(b.<Float> get("unitPrice"), bookCriteria.getMaxPrice()));
	}

	query.select(b).where(predicates.toArray(new Predicate[] {}));

	return em.createQuery(query).getResultList();
    }
}
