package edu.flst.bookstore.domaine.bo;

import java.util.Collection;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Projet Bookstore (FST) Créé par Dimitri le 08/11/14.
 */
@Entity
@Table(name = "USERS")
public class User {

    /**
     * The login.
     */
    private String login;

    /** The email. */
    private String email;

    /** The password. */
    private String password;

    /** The orders. */
    private Collection<Order> orders;

    /** The mailing address. */
    private MailingAddress mailingAddress;

    /**
     * Gets login.
     *
     * @return the login
     */
    @Id
    @Column(name = "LOGIN")
    public String getLogin() {
	return login;
    }

    /**
     * Sets login.
     *
     * @param login
     *            the login
     */
    public void setLogin(String login) {
	this.login = login;
    }

    /**
     * Gets email.
     *
     * @return the email
     */
    @Basic
    @Column(name = "EMAIL")
    public String getEmail() {
	return email;
    }

    /**
     * Sets email.
     *
     * @param email
     *            the email
     */
    public void setEmail(String email) {
	this.email = email;
    }

    /**
     * Gets password.
     *
     * @return the password
     */
    @Basic
    @Column(name = "PWD")
    public String getPassword() {
	return password;
    }

    /**
     * Sets password.
     *
     * @param pwd
     *            the pwd
     */
    public void setPassword(String pwd) {
	this.password = pwd;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object o) {
	if (this == o) {
	    return true;
	}
	if (o == null || getClass() != o.getClass()) {
	    return false;
	}

	User user = (User) o;

	if (email != null ? !email.equals(user.email) : user.email != null) {
	    return false;
	}
	if (login != null ? !login.equals(user.login) : user.login != null) {
	    return false;
	}
	if (password != null ? !password.equals(user.password) : user.password != null) {
	    return false;
	}

	return true;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
	int result = login != null ? login.hashCode() : 0;
	result = 31 * result + (email != null ? email.hashCode() : 0);
	result = 31 * result + (password != null ? password.hashCode() : 0);
	return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
	return "User{" + "login='" + login + '\'' + ", email='" + email + '\'' + ", password='" + password + '\'' + ", mailingAddress="
	        + mailingAddress + '}';
    }

    /**
     * Gets orders.
     *
     * @return the orders
     */
    @OneToMany(mappedBy = "user")
    public Collection<Order> getOrders() {
	return orders;
    }

    /**
     * Sets orders.
     *
     * @param ordersesByLogin
     *            the orderses by login
     */
    public void setOrders(Collection<Order> ordersesByLogin) {
	this.orders = ordersesByLogin;
    }

    /**
     * Gets mailing address.
     *
     * @return the mailing address
     */
    @ManyToOne
    @JoinColumn(name = "PERSONNAL_ADR_ID", referencedColumnName = "ID")
    public MailingAddress getMailingAddress() {
	return mailingAddress;
    }

    /**
     * Sets mailing address.
     *
     * @param mailingAddressByPersonnalAdrId
     *            the mailing address by personnal adr id
     */
    public void setMailingAddress(MailingAddress mailingAddressByPersonnalAdrId) {
	this.mailingAddress = mailingAddressByPersonnalAdrId;
    }
}
