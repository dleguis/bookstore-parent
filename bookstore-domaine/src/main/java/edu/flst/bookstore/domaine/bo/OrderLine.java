package edu.flst.bookstore.domaine.bo;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Created by Dimitri on 08/11/14.
 */
@Entity
@Table(name = "ORDER_LINES", schema = "")
@IdClass(OrderLinesPK.class)
public class OrderLine {

    /**
     * The quantity.
     */
    private int quantity;

    /** The order id. */
    private Integer orderId;

    /** The book id. */
    private String bookId;

    /** The book. */
    private Book book;

    /** The order. */
    private Order order;

    /**
     * Gets quantity.
     *
     * @return the quantity
     */
    @Basic
    @Column(name = "QUANTITY")
    public int getQuantity() {
	return quantity;
    }

    /**
     * Sets quantity.
     *
     * @param quantity
     *            the quantity
     */
    public void setQuantity(int quantity) {
	this.quantity = quantity;
    }

    /**
     * Gets order id.
     *
     * @return the order id
     */
    @Id
    @Column(name = "ORDERS_ID", insertable = false, updatable = false)
    public Integer getOrderId() {
	return orderId;
    }

    /**
     * Sets order id.
     *
     * @param ordersId
     *            the orders id
     */
    public void setOrderId(Integer ordersId) {
	this.orderId = ordersId;
    }

    /**
     * Gets book id.
     *
     * @return the book id
     */
    @Id
    @Column(name = "BOOKS_ID", insertable = false, updatable = false)
    public String getBookId() {
	return bookId;
    }

    /**
     * Sets book id.
     *
     * @param booksId
     *            the books id
     */
    public void setBookId(String booksId) {
	this.bookId = booksId;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object o) {
	if (this == o) {
	    return true;
	}
	if (o == null || getClass() != o.getClass()) {
	    return false;
	}

	OrderLine that = (OrderLine) o;

	if (!orderId.equals(that.orderId)) {
	    return false;
	}
	if (quantity != that.quantity) {
	    return false;
	}
	if (bookId != null ? !bookId.equals(that.bookId) : that.bookId != null) {
	    return false;
	}

	return true;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
	int result = quantity;
	result = 31 * result + orderId;
	result = 31 * result + (bookId != null ? bookId.hashCode() : 0);
	return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
	return "OrderLine{" + "quantity=" + quantity + ", orderId=" + orderId + ", bookId='" + bookId + '\'' + ", book=" + book + '}';
    }

    /**
     * Gets book.
     *
     * @return the book
     */
    @ManyToOne
    @JoinColumn(name = "BOOKS_ID", referencedColumnName = "ISBN13", nullable = false, insertable = false, updatable = false)
    public Book getBook() {
	return book;
    }

    /**
     * Sets book.
     *
     * @param booksByBookId
     *            the books by book id
     */
    public void setBook(Book booksByBookId) {
	this.book = booksByBookId;
    }

    /**
     * Gets order.
     *
     * @return the order
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ORDERS_ID", referencedColumnName = "ID", nullable = false, insertable = false, updatable = false)
    public Order getOrder() {
	return order;
    }

    /**
     * Sets order.
     *
     * @param ordersByOrderId
     *            the orders by order id
     */
    public void setOrder(Order ordersByOrderId) {
	this.order = ordersByOrderId;
    }
}
