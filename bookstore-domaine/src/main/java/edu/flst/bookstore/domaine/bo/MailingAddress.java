package edu.flst.bookstore.domaine.bo;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by Dimitri on 08/11/14.
 */
@Entity
@Table(name = "MAILING_ADDRESSES")
public class MailingAddress {

    /**
     * The id.
     */
    private Integer id;

    /** The line3. */
    private String line3;

    /** The zip. */
    private String zip;

    /** The line1. */
    private String line1;

    /** The line2. */
    private String line2;

    /** The city. */
    private String city;

    /**
     * Gets id.
     *
     * @return the id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    public Integer getId() {
	return id;
    }

    /**
     * Sets id.
     *
     * @param id
     *            the id
     */
    public void setId(Integer id) {
	this.id = id;
    }

    /**
     * Gets line 3.
     *
     * @return the line 3
     */
    @Basic
    @Column(name = "LINE3")
    public String getLine3() {
	return line3;
    }

    /**
     * Sets line 3.
     *
     * @param line3
     *            the line 3
     */
    public void setLine3(String line3) {
	this.line3 = line3;
    }

    /**
     * Gets zip.
     *
     * @return the zip
     */
    @Basic
    @Column(name = "ZIP")
    public String getZip() {
	return zip;
    }

    /**
     * Sets zip.
     *
     * @param zip
     *            the zip
     */
    public void setZip(String zip) {
	this.zip = zip;
    }

    /**
     * Gets line 1.
     *
     * @return the line 1
     */
    @Basic
    @Column(name = "LINE1")
    public String getLine1() {
	return line1;
    }

    /**
     * Sets line 1.
     *
     * @param line1
     *            the line 1
     */
    public void setLine1(String line1) {
	this.line1 = line1;
    }

    /**
     * Gets line 2.
     *
     * @return the line 2
     */
    @Basic
    @Column(name = "LINE2")
    public String getLine2() {
	return line2;
    }

    /**
     * Sets line 2.
     *
     * @param line2
     *            the line 2
     */
    public void setLine2(String line2) {
	this.line2 = line2;
    }

    /**
     * Gets city.
     *
     * @return the city
     */
    @Basic
    @Column(name = "CITY")
    public String getCity() {
	return city;
    }

    /**
     * Sets city.
     *
     * @param city
     *            the city
     */
    public void setCity(String city) {
	this.city = city;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object o) {
	if (this == o) {
	    return true;
	}
	if (o == null || getClass() != o.getClass()) {
	    return false;
	}

	MailingAddress that = (MailingAddress) o;

	if (id != that.id) {
	    return false;
	}
	if (city != null ? !city.equals(that.city) : that.city != null) {
	    return false;
	}
	if (line1 != null ? !line1.equals(that.line1) : that.line1 != null) {
	    return false;
	}
	if (line2 != null ? !line2.equals(that.line2) : that.line2 != null) {
	    return false;
	}
	if (line3 != null ? !line3.equals(that.line3) : that.line3 != null) {
	    return false;
	}
	if (zip != null ? !zip.equals(that.zip) : that.zip != null) {
	    return false;
	}

	return true;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
	int result = id;
	result = 31 * result + (line3 != null ? line3.hashCode() : 0);
	result = 31 * result + (zip != null ? zip.hashCode() : 0);
	result = 31 * result + (line1 != null ? line1.hashCode() : 0);
	result = 31 * result + (line2 != null ? line2.hashCode() : 0);
	result = 31 * result + (city != null ? city.hashCode() : 0);
	return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
	return "MailingAddress{" + "id=" + id + ", line3='" + line3 + '\'' + ", zip='" + zip + '\'' + ", line1='" + line1 + '\''
	        + ", line2='" + line2 + '\'' + ", city='" + city + '\'' + '}';
    }

}
