package edu.flst.bookstore.domaine.bo;

import java.util.Collection;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Created by Dimitri on 08/11/14.
 */
@Entity
@Table(name = "ORDERS")
public class Order {

    /**
     * The id.
     */
    private Integer id;

    /** The order date. */
    private Date orderDate;

    /** The mailing address. */
    private MailingAddress mailingAddress;

    /** The user. */
    private User user;

    /** The order lines. */
    private Collection<OrderLine> orderLines;

    /**
     * Gets id.
     *
     * @return the id
     */
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer getId() {
	return id;
    }

    /**
     * Sets id.
     *
     * @param id
     *            the id
     */
    public void setId(Integer id) {
	this.id = id;
    }

    /**
     * Gets order date.
     *
     * @return the order date
     */
    @Basic
    @Column(name = "ORDER_DATE")
    public Date getOrderDate() {
	return orderDate;
    }

    /**
     * Sets order date.
     *
     * @param orderDate
     *            the order date
     */
    public void setOrderDate(Date orderDate) {
	this.orderDate = orderDate;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object o) {
	if (this == o) {
	    return true;
	}
	if (o == null || getClass() != o.getClass()) {
	    return false;
	}

	Order order = (Order) o;

	if (id != order.id) {
	    return false;
	}
	if (orderDate != null ? !orderDate.equals(order.orderDate) : order.orderDate != null) {
	    return false;
	}
	return true;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
	int result = id;
	result = 31 * result + (orderDate != null ? orderDate.hashCode() : 0);
	return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
	return "Order{" + "id=" + id + ", orderDate=" + orderDate + ", mailingAddress=" + mailingAddress + ", orderLines=" + orderLines
	        + '}';
    }

    /**
     * Gets mailing address.
     *
     * @return the mailing address
     */
    @ManyToOne
    @JoinColumn(name = "SHIPPING_ADR_ID", referencedColumnName = "ID")
    public MailingAddress getMailingAddress() {
	return mailingAddress;
    }

    /**
     * Sets mailing address.
     *
     * @param mailingAddressByShippingAdrId
     *            the mailing address by shipping adr id
     */
    public void setMailingAddress(MailingAddress mailingAddressByShippingAdrId) {
	this.mailingAddress = mailingAddressByShippingAdrId;
    }

    /**
     * Gets user.
     *
     * @return the user
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "USER_ID", referencedColumnName = "LOGIN")
    public User getUser() {
	return user;
    }

    /**
     * Sets user.
     *
     * @param userByUserId
     *            the user by user id
     */
    public void setUser(User userByUserId) {
	this.user = userByUserId;
    }

    /**
     * Gets order lines.
     *
     * @return the order lines
     */
    @OneToMany(mappedBy = "order", fetch = FetchType.EAGER)
    public Collection<OrderLine> getOrderLines() {
	return orderLines;
    }

    /**
     * Sets order lines.
     *
     * @param orderLinesesById
     *            the order lineses by id
     */
    public void setOrderLines(Collection<OrderLine> orderLinesesById) {
	this.orderLines = orderLinesesById;
    }
}
