package edu.flst.bookstore.domaine.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import edu.flst.bookstore.domaine.bo.User;

/**
 * Created by Dimitri on 19/10/14.
 */
public interface UserRepository extends JpaRepository<User, String> {

    /**
     * Find by login and password.
     *
     * @param login    the login
     * @param password the password
     * @return the user
     */
    User findByLoginAndPassword(String login, String password);

    /**
     * Find by email.
     *
     * @param email the email
     * @return the user
     */
    User findByEmail(String email);

    /**
     * Find one deep.
     *
     * @param login the login
     * @return the user
     */
    @Query("SELECT u FROM User u LEFT JOIN FETCH u.orders WHERE u.login = :login")
    User findOneDeep(@Param("login") String login);
}
