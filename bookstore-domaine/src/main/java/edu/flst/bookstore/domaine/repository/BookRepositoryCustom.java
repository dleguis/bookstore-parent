package edu.flst.bookstore.domaine.repository;

import java.util.List;

import edu.flst.bookstore.domaine.bo.Book;
import edu.flst.bookstore.transverse.builder.BookCriteria;

/**
 * Created by Dimitri on 14/01/2015.
 */
public interface BookRepositoryCustom {
    /**
     * Find by criteria.
     *
     * @param bookCriteria the book criteria
     * @return the list
     */
    List<Book> findByCriteria(BookCriteria bookCriteria);
}
