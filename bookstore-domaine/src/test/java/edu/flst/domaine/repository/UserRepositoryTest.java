package edu.flst.domaine.repository;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import edu.flst.bookstore.domaine.repository.UserRepository;

/**
 * Created by Dimitri on 20/10/14.
 */
@ContextConfiguration("classpath*:config/bookstore-domaine-datasource-test.xml")
@RunWith(SpringJUnit4ClassRunner.class)
public class UserRepositoryTest {

    /**
     * The user repository.
     */
    @Autowired
    private UserRepository userRepository;

    /**
     * Find one test.
     */
    @Test
    public void findOneTest() {
	assertNotNull(userRepository.findOne("yhovart"));
    }
}
