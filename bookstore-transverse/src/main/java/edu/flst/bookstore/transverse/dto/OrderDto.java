package edu.flst.bookstore.transverse.dto;

import java.util.Collection;
import java.util.Date;

/**
 * Created by Dimitri on 16/11/14.
 */
public class OrderDto {

    /**
     * The id.
     */
    private Integer id;

    /** The order date. */
    private Date orderDate;

    /** The mailing address. */
    private MailingAddressDto mailingAddress;

    /** The user. */
    private UserDto user;

    /** The order lines. */
    private Collection<OrderLineDto> orderLines;

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Integer getId() {
	return id;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(Integer id) {
	this.id = id;
    }

    /**
     * Gets the order date.
     *
     * @return the order date
     */
    public Date getOrderDate() {
	return orderDate;
    }

    /**
     * Sets the order date.
     *
     * @param orderDate the new order date
     */
    public void setOrderDate(Date orderDate) {
	this.orderDate = orderDate;
    }

    /**
     * Gets the mailing address.
     *
     * @return the mailing address
     */
    public MailingAddressDto getMailingAddress() {
	return mailingAddress;
    }

    /**
     * Sets the mailing address.
     *
     * @param mailingAddress the new mailing address
     */
    public void setMailingAddress(MailingAddressDto mailingAddress) {
	this.mailingAddress = mailingAddress;
    }

    /**
     * Gets the user.
     *
     * @return the user
     */
    public UserDto getUser() {
	return user;
    }

    /**
     * Sets the user.
     *
     * @param user the new user
     */
    public void setUser(UserDto user) {
	this.user = user;
    }

    /**
     * Gets the order lines.
     *
     * @return the order lines
     */
    public Collection<OrderLineDto> getOrderLines() {
	return orderLines;
    }

    /**
     * Sets the order lines.
     *
     * @param orderLines the new order lines
     */
    public void setOrderLines(Collection<OrderLineDto> orderLines) {
	this.orderLines = orderLines;
    }
}
