package edu.flst.bookstore.transverse.dto;

import java.util.Collection;

/**
 * Created by Dimitri on 02/11/14.
 */
public class UserDto {

    /**
     * The login.
     */
    private String login;

    /** The email. */
    private String email;

    /** The password. */
    private String password;

    /** The orders. */
    private Collection<OrderDto> orders;

    /** The mailing address. */
    private MailingAddressDto mailingAddress;

    /**
     * Gets the login.
     *
     * @return the login
     */
    public String getLogin() {
	return login;
    }

    /**
     * Sets the login.
     *
     * @param login the new login
     */
    public void setLogin(String login) {
	this.login = login;
    }

    /**
     * Gets the email.
     *
     * @return the email
     */
    public String getEmail() {
	return email;
    }

    /**
     * Sets the email.
     *
     * @param email the new email
     */
    public void setEmail(String email) {
	this.email = email;
    }

    /**
     * Gets the password.
     *
     * @return the password
     */
    public String getPassword() {
	return password;
    }

    /**
     * Sets the password.
     *
     * @param password the new password
     */
    public void setPassword(String password) {
	this.password = password;
    }

    /**
     * Gets the orders.
     *
     * @return the orders
     */
    public Collection<OrderDto> getOrders() {
	return orders;
    }

    /**
     * Sets the orders.
     *
     * @param orders the new orders
     */
    public void setOrders(Collection<OrderDto> orders) {
	this.orders = orders;
    }

    /**
     * Gets the mailing address.
     *
     * @return the mailing address
     */
    public MailingAddressDto getMailingAddress() {
	return mailingAddress;
    }

    /**
     * Sets the mailing address.
     *
     * @param mailingAddress the new mailing address
     */
    public void setMailingAddress(MailingAddressDto mailingAddress) {
	this.mailingAddress = mailingAddress;
    }
}
