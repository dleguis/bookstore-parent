package edu.flst.bookstore.transverse.exception;

import edu.flst.bookstore.transverse.exception.code.BookstoreFonctionnelleExceptionCode;

/**
 * Created by Dimitri on 16/11/14.
 */
public class BookstoreFonctionnelleException extends RuntimeException {

    /**
     * The exception cause.
     */
    BookstoreFonctionnelleExceptionCode exceptionCause;

    /**
     * Instantiates a new bookstore fonctionnelle exception.
     *
     * @param cause the cause
     */
    public BookstoreFonctionnelleException(BookstoreFonctionnelleExceptionCode cause) {
	super(cause.getMessage());
	this.exceptionCause = cause;
    }

    /**
     * Gets the exception cause.
     *
     * @return the exception cause
     */
    public BookstoreFonctionnelleExceptionCode getExceptionCause() {
	return exceptionCause;
    }
}
