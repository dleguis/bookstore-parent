package edu.flst.bookstore.transverse.dto;

/**
 * Created by Dimitri on 16/11/14.
 */
public class OrderLineDto {

    /**
     * The quantity.
     */
    private int quantity;

    /** The order id. */
    private Integer orderId;

    /** The book id. */
    private String bookId;

    /** The book. */
    private BookDto book;

    /** The order. */
    private OrderDto order;

    /**
     * Gets the quantity.
     *
     * @return the quantity
     */
    public int getQuantity() {
	return quantity;
    }

    /**
     * Sets the quantity.
     *
     * @param quantity the new quantity
     */
    public void setQuantity(int quantity) {
	this.quantity = quantity;
    }

    /**
     * Gets the order id.
     *
     * @return the order id
     */
    public Integer getOrderId() {
	return orderId;
    }

    /**
     * Sets the order id.
     *
     * @param orderId the new order id
     */
    public void setOrderId(Integer orderId) {
	this.orderId = orderId;
    }

    /**
     * Gets the book id.
     *
     * @return the book id
     */
    public String getBookId() {
	return bookId;
    }

    /**
     * Sets the book id.
     *
     * @param bookId the new book id
     */
    public void setBookId(String bookId) {
	this.bookId = bookId;
    }

    /**
     * Gets the book.
     *
     * @return the book
     */
    public BookDto getBook() {
	return book;
    }

    /**
     * Sets the book.
     *
     * @param book the new book
     */
    public void setBook(BookDto book) {
	this.book = book;
    }

    /**
     * Gets the order.
     *
     * @return the order
     */
    public OrderDto getOrder() {
	return order;
    }

    /**
     * Sets the order.
     *
     * @param order the new order
     */
    public void setOrder(OrderDto order) {
	this.order = order;
    }

}
