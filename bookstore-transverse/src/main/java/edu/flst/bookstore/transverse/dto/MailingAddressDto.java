package edu.flst.bookstore.transverse.dto;

import java.util.Collection;

/**
 * Created by Dimitri on 16/11/14.
 */
public class MailingAddressDto {

    /**
     * The id.
     */
    private int id;

    /** The line3. */
    private String line3;

    /** The zip. */
    private String zip;

    /** The line1. */
    private String line1;

    /** The line2. */
    private String line2;

    /** The city. */
    private String city;

    /** The orders. */
    private Collection<OrderDto> orders;

    /** The users. */
    private Collection<UserDto> users;

    /**
     * Gets the id.
     *
     * @return the id
     */
    public int getId() {
	return id;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(int id) {
	this.id = id;
    }

    /**
     * Gets the line3.
     *
     * @return the line3
     */
    public String getLine3() {
	return line3;
    }

    /**
     * Sets the line3.
     *
     * @param line3 the new line3
     */
    public void setLine3(String line3) {
	this.line3 = line3;
    }

    /**
     * Gets the zip.
     *
     * @return the zip
     */
    public String getZip() {
	return zip;
    }

    /**
     * Sets the zip.
     *
     * @param zip the new zip
     */
    public void setZip(String zip) {
	this.zip = zip;
    }

    /**
     * Gets the line1.
     *
     * @return the line1
     */
    public String getLine1() {
	return line1;
    }

    /**
     * Sets the line1.
     *
     * @param line1 the new line1
     */
    public void setLine1(String line1) {
	this.line1 = line1;
    }

    /**
     * Gets the line2.
     *
     * @return the line2
     */
    public String getLine2() {
	return line2;
    }

    /**
     * Sets the line2.
     *
     * @param line2 the new line2
     */
    public void setLine2(String line2) {
	this.line2 = line2;
    }

    /**
     * Gets the city.
     *
     * @return the city
     */
    public String getCity() {
	return city;
    }

    /**
     * Sets the city.
     *
     * @param city the new city
     */
    public void setCity(String city) {
	this.city = city;
    }

    /**
     * Gets the orders.
     *
     * @return the orders
     */
    public Collection<OrderDto> getOrders() {
	return orders;
    }

    /**
     * Sets the orders.
     *
     * @param orders the new orders
     */
    public void setOrders(Collection<OrderDto> orders) {
	this.orders = orders;
    }

    /**
     * Gets the users.
     *
     * @return the users
     */
    public Collection<UserDto> getUsers() {
	return users;
    }

    /**
     * Sets the users.
     *
     * @param users the new users
     */
    public void setUsers(Collection<UserDto> users) {
	this.users = users;
    }
}
