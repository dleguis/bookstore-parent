package edu.flst.bookstore.transverse.dto;

import java.sql.Date;
import java.util.Collection;

/**
 * Created by Dimitri on 16/11/14.
 */
public class AuthorDto {

    /**
     * The id.
     */
    private Integer id;

    /** The last name. */
    private String lastName;

    /** The birth date. */
    private Date birthDate;

    /** The first name. */
    private String firstName;

    /** The books. */
    private Collection<BookDto> books;

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Integer getId() {
	return id;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(Integer id) {
	this.id = id;
    }

    /**
     * Gets the last name.
     *
     * @return the last name
     */
    public String getLastName() {
	return lastName;
    }

    /**
     * Sets the last name.
     *
     * @param lastName the new last name
     */
    public void setLastName(String lastName) {
	this.lastName = lastName;
    }

    /**
     * Gets the birth date.
     *
     * @return the birth date
     */
    public Date getBirthDate() {
	return birthDate;
    }

    /**
     * Sets the birth date.
     *
     * @param birthDate the new birth date
     */
    public void setBirthDate(Date birthDate) {
	this.birthDate = birthDate;
    }

    /**
     * Gets the first name.
     *
     * @return the first name
     */
    public String getFirstName() {
	return firstName;
    }

    /**
     * Sets the first name.
     *
     * @param firstName the new first name
     */
    public void setFirstName(String firstName) {
	this.firstName = firstName;
    }

    /**
     * Gets the books.
     *
     * @return the books
     */
    public Collection<BookDto> getBooks() {
	return books;
    }

    /**
     * Sets the books.
     *
     * @param books the new books
     */
    public void setBooks(Collection<BookDto> books) {
	this.books = books;
    }
}
