package edu.flst.bookstore.transverse.dto;

import java.util.Collection;

/**
 * Created by Dimitri on 16/11/14.
 */
public class BookDto {

    /**
     * The isbn13.
     */
    private String isbn13;

    /** The title. */
    private String title;

    /** The editor. */
    private String editor;

    /** The unit price. */
    private Float unitPrice;

    /** The author. */
    private AuthorDto author;

    /** The order lines. */
    private Collection<OrderLineDto> orderLines;

    /**
     * Gets the isbn13.
     *
     * @return the isbn13
     */
    public String getIsbn13() {
	return isbn13;
    }

    /**
     * Sets the isbn13.
     *
     * @param isbn13 the new isbn13
     */
    public void setIsbn13(String isbn13) {
	this.isbn13 = isbn13;
    }

    /**
     * Gets the title.
     *
     * @return the title
     */
    public String getTitle() {
	return title;
    }

    /**
     * Sets the title.
     *
     * @param title the new title
     */
    public void setTitle(String title) {
	this.title = title;
    }

    /**
     * Gets the editor.
     *
     * @return the editor
     */
    public String getEditor() {
	return editor;
    }

    /**
     * Sets the editor.
     *
     * @param editor the new editor
     */
    public void setEditor(String editor) {
	this.editor = editor;
    }

    /**
     * Gets the unit price.
     *
     * @return the unit price
     */
    public Float getUnitPrice() {
	return unitPrice;
    }

    /**
     * Sets the unit price.
     *
     * @param unitPrice the new unit price
     */
    public void setUnitPrice(Float unitPrice) {
	this.unitPrice = unitPrice;
    }

    /**
     * Gets the author.
     *
     * @return the author
     */
    public AuthorDto getAuthor() {
	return author;
    }

    /**
     * Sets the author.
     *
     * @param author the new author
     */
    public void setAuthor(AuthorDto author) {
	this.author = author;
    }

    /**
     * Gets the order lines.
     *
     * @return the order lines
     */
    public Collection<OrderLineDto> getOrderLines() {
	return orderLines;
    }

    /**
     * Sets the order lines.
     *
     * @param orderLines the new order lines
     */
    public void setOrderLines(Collection<OrderLineDto> orderLines) {
	this.orderLines = orderLines;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object o) {
	if (this == o) {
	    return true;
	}
	if (!(o instanceof BookDto)) {
	    return false;
	}

	BookDto bookDto = (BookDto) o;

	if (!isbn13.equals(bookDto.isbn13)) {
	    return false;
	}

	return true;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
	return isbn13.hashCode();
    }
}
